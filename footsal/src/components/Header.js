import React from 'react';
import { Link } from 'react-router-dom';
import {
    Breadcrumb,
    BreadcrumbItem,
    BreadcrumbLink,
    BreadcrumbSeparator,
  } from '@chakra-ui/react'

function Header(props) {
    return (
        <Breadcrumb>
            <BreadcrumbItem>
            <Link to="/">
                <h1>Home.</h1>
            </Link>
            </BreadcrumbItem>

            <BreadcrumbItem>
            <Link to="/management">
                <h1>Management</h1>
            </Link>
            </BreadcrumbItem>

            <BreadcrumbItem isCurrentPage>
            <Link to="/team_matching">
                <h1>Team Matching</h1>
            </Link>
            </BreadcrumbItem>
        </Breadcrumb>
    );
}

export default Header;
