import React, { Component } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Header from './components/Header';

import Memberlist from './components/memberlist';
import Management from './components/management';
import TeamMatching from './components/team_matching';
import { ChakraProvider } from '@chakra-ui/react'


const App = () => {
	return (
		<ChakraProvider>
			<BrowserRouter>
				<Header />
				<Routes>
          <Route path="/" element={<Memberlist />}></Route>
          <Route path="/team_matching" element={<TeamMatching />}></Route>
          <Route path="/management" element={<Management />}></Route>
				</Routes>
			</BrowserRouter>
    </ChakraProvider>
	);
};

export default App;
