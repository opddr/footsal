package com.footsal.footsal;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
public interface UsersRepository extends CrudRepository<Users, Long>
{
	Users findByName(String name);
}