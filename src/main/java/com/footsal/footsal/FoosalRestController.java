package com.footsal.footsal;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FoosalRestController {
	
	@Autowired
	UsersRepository repo;
	
	@GetMapping("/get")
	public String Get(@RequestParam(value="name",defaultValue="World") String name)
	{
		
		return repo.findByName(name).toString();
		
	}
	
	@GetMapping("/save")
	public String save(
			@RequestParam(value="name") String name,
			@RequestParam(value="phone") String phone,
			@RequestParam(value="aq") String aq,
			@RequestParam(value="dq") String dq
			)
	{
		Users user = new Users(name,phone,aq,dq);
		repo.save(user);
		return "hi";
		
	}
	
}
