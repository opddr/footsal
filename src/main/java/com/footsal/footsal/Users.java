package com.footsal.footsal;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
public class Users {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String name;
	private String phone;
	private String aq;
	private String dq;
	
	protected Users() {}
	
	public Users(String name,String phone,String aq,String dq)
	{
		this.name = name;
		this.phone = phone;
		this.aq = aq;
		this.dq = dq;
		
	}
	
	@Override
	public String toString()
	{
		
		return String.format("name:%s, phone:%s, dq:%s, dq:%s",name,phone,aq,dq);
	}
	


}
