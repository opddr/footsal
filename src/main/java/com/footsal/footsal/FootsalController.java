package com.footsal.footsal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class FootsalController {

	@Autowired
	UsersRepository userRepository;
	@GetMapping("/greeting")
	public String greeting(
		@RequestParam(name="name",
		required=false,
		defaultValue="World")
		String name,
		Model model
	)
	{
		model.addAttribute("name","asd");
		return "index";
	}
}
